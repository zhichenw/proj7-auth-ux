# Laptop Service
import os

from flask import Flask, jsonify, request
from flask_restful import Resource, Api
from flask_cors import CORS

from pymongo import MongoClient

import web_token, password as pass_hash

client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)
db = client.acp_time

# Instantiate the app
app = Flask(__name__)
api = Api(app)
CORS(app)


def list_dic_to_csv(list_dic, heads):
    csv = ''
    for head in heads:
        csv += head + ', '
        if head == heads[len(heads) - 1]:
            csv = csv[:-2]
            csv += '\n'

    for dic in list_dic:
        for head in heads:
            csv += dic[head] + ', '
            if head == heads[len(heads) - 1]:
                csv = csv[:-2]
                csv += '\n'
    
    return csv

def del_list_dic_key(list_dic, key):
    new_list_dic = []
    for dic in list_dic:
        del dic[key]
        new_list_dic.append(dic)
    return new_list_dic

def token_required(func):
    def wrapper(*args, **kwargs):
        token = request.args.get('token', '')
        if token == '':
            res = jsonify(result={"message": "unauthorized action"})
            res.status_code = 401
            return res
        else:
            if web_token.verify_auth_token(token):
                return func(*args, **kwargs)
            else:
                res = jsonify(result={"message": "unauthorized action"})
                res.status_code = 401
                return res
    return wrapper

"""All"""

class acp_time_all(Resource):

    @token_required
    def get(self):
        n = db.control_times.find().count()
        if n == 0:
            return jsonify(result={})
        else:
            control_times = db.control_times.find().limit(1)[0]
        controls = control_times['controls']

        return jsonify(result=controls)

class acp_time_all_json(Resource):

    @token_required
    def get(self):
        n = db.control_times.find().count()
        if n == 0:
            return jsonify(result={})
        else:
            control_times = db.control_times.find().limit(1)[0]
        controls = control_times['controls']

        return jsonify(result=controls)

class acp_time_all_csv(Resource):

    @token_required
    def get(self):
        n = db.control_times.find().count()
        if n == 0:
            return jsonify(result={})
        else:
            control_times = db.control_times.find().limit(1)[0]
        csv_heads = ['miles', 'kms', 'location', 'open', 'close']
        controls_csv = list_dic_to_csv(control_times['controls'], csv_heads)

        return controls_csv

"""OpenOnly"""

class acp_time_openOnly(Resource):

    @token_required
    def get(self):
        top_n = int(request.args['top']) if 'top' in request.args.keys() else None
        n = db.control_times.find().count()
        if n == 0:
            return jsonify(result={})
        else:
            control_times = db.control_times.find().limit(1)[0]
        controls = del_list_dic_key(control_times['controls'], 'close')[:top_n]

        return jsonify(result=controls)

class acp_time_openOnly_json(Resource):

    @token_required
    def get(self):
        top_n = int(request.args['top']) if 'top' in request.args.keys() else None
        n = db.control_times.find().count()
        if n == 0:
            return jsonify(result={})
        else:
            control_times = db.control_times.find().limit(1)[0]
        controls = del_list_dic_key(control_times['controls'], 'close')[:top_n]

        return jsonify(result=controls)

class acp_time_openOnly_csv(Resource):

    @token_required
    def get(self):
        top_n = int(request.args['top']) if 'top' in request.args.keys() else None
        n = db.control_times.find().count()
        if n == 0:
            return jsonify(result={})
        else:
            control_times = db.control_times.find().limit(1)[0]
        csv_heads = ['miles', 'kms', 'location', 'open']
        controls_csv = list_dic_to_csv(del_list_dic_key(control_times['controls'], 'close')[:top_n], csv_heads)
        
        return controls_csv

"""CloseOnly"""

class acp_time_closeOnly(Resource):

    @token_required
    def get(self):
        top_n = int(request.args['top']) if 'top' in request.args.keys() else None
        n = db.control_times.find().count()
        if n == 0:
            return jsonify(result={})
        else:
            control_times = db.control_times.find().limit(1)[0]
        controls = del_list_dic_key(control_times['controls'], 'open')[:top_n]
        
        return jsonify(result=controls)

class acp_time_closeOnly_json(Resource):

    @token_required
    def get(self):
        top_n = int(request.args['top']) if 'top' in request.args.keys() else None
        n = db.control_times.find().count()
        if n == 0:
            return jsonify(result={})
        else:
            control_times = db.control_times.find().limit(1)[0]
        controls = del_list_dic_key(control_times['controls'], 'open')[:top_n]

        return jsonify(result=controls)

class acp_time_closeOnly_csv(Resource):

    @token_required
    def get(self):
        top_n = int(request.args['top']) if 'top' in request.args.keys() else None
        n = db.control_times.find().count()
        if n == 0:
            return jsonify(result={})
        else:
            control_times = db.control_times.find().limit(1)[0]
        csv_heads = ['miles', 'kms', 'location', 'close']
        controls_csv = list_dic_to_csv(del_list_dic_key(control_times['controls'], 'open')[:top_n], csv_heads)
        
        return controls_csv

"""auth"""

@app.route('/api/register', methods=["POST"])
def register_user():
    register_info = request.json
    if not ('username' in register_info.keys() and 'password' in register_info.keys()):
        return jsonify(result={"message": "please provide username and password"}), 400
    n_users = db.users.find().count()
    user_id = n_users + 1
    user = {
        "id": user_id,
        "username": register_info['username'],
        "password": pass_hash.hash_password(register_info['password'])
    }
    db.users.insert_one(user)
    del user['_id']
    res = jsonify(result=user)
    res.headers['Location'] = '/api/users/' + str(user_id)
    return res, 201

@app.route('/api/token', methods=["GET"])
def get_token():
    username = request.args.get('username', '')
    password = request.args.get('password', '')
    if username is '' or password is '':
        return jsonify(result={"message": "please provide username and password"}), 400
    user = db.users.find({'username': username})
    hashed_val = user[0]['password']
    user_id = user[0]['id']
    if pass_hash.verify_password(password, hashed_val):
        token = str(web_token.generate_auth_token(user_id))[2:-1]
        return jsonify(result={"token": str(token), 'duration': 600})
    return jsonify(result={"message": "unauthorized username and password"}), 401

# Create routes
#part 1
api.add_resource(acp_time_all, '/listAll')
api.add_resource(acp_time_openOnly, '/listOpenOnly')
api.add_resource(acp_time_closeOnly, '/listCloseOnly')

#part 2
api.add_resource(acp_time_all_csv, '/listAll/csv')
api.add_resource(acp_time_openOnly_csv, '/listOpenOnly/csv')
api.add_resource(acp_time_closeOnly_csv, '/listCloseOnly/csv')
api.add_resource(acp_time_all_json, '/listAll/json')
api.add_resource(acp_time_openOnly_json, '/listOpenOnly/json')
api.add_resource(acp_time_closeOnly_json, '/listCloseOnly/json')


# Run the application
if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5001, debug=True)
