import os
from flask import Flask, redirect, url_for, request, render_template, jsonify, flash
from pymongo import MongoClient
import arrow
import acp_times
import forms
from config import Config

from flask_login import (LoginManager, current_user, login_required,
                           login_user, logout_user, UserMixin, 
                            confirm_login, fresh_login_required)

app = Flask(__name__)
app.config.from_object(Config)

client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)
db = client.acp_time

login_manager = LoginManager()
login_manager.login_view = "login"
login_manager.login_message = u"Please log in to access this page."

class User(UserMixin):
    def __init__(self, name, id, active=True):
        self.name = name
        self.id = id
        self.active = active

    def is_active(self):
        return self.active

@login_manager.user_loader
def load_user(id):
    username = db.users.find({'id': int(id)})[0]['username']
    user_id = id
    user = User(username, user_id)
    return user

login_manager.setup_app(app)

@app.route("/")
@app.route("/index")
@fresh_login_required
def index():
    return render_template('calc.html')

@app.route("/browse-api")
@fresh_login_required
def browse_api():
    return render_template('browse-api.html')

@app.route("/register", methods=["GET"])
def register():
    form = forms.RegisterForm()
    return render_template('register.html', form=form)

@app.route("/login", methods=["GET", "POST"])
def login():
    form = forms.LoginForm()
    if request.method == "POST":
        form = forms.LoginForm(request.form)
        if form.validate():
            username = form.username.data
            try:
                user_id = db.users.find({'username': username})[0]['id']
            except:
                flash("Sorry, but you could not log in.")
                return render_template('login.html', form=form)
            remember = request.form.get("remember", "no") == "yes"
            user = User(username, user_id)
            if login_user(user, remember=remember):
                flash("Logged in!")
                return redirect(request.args.get("next") or url_for("index"))
            else:
                flash("Sorry, but you could not log in.")
    return render_template('login.html', form=form)

@app.route("/logout")
@fresh_login_required
def logout():
    logout_user()
    flash("Logged out.")
    return redirect(url_for("index"))

@app.errorhandler(404)
def page_not_found(error):
    return render_template('404.html'), 404

@app.route("/_calc_times")
@fresh_login_required
def _calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    km = request.args.get('km', 999, type=float)
    distance = request.args.get('distance', type=int)
    start_time = request.args.get('start_time')
    open_time = acp_times.open_time(km, distance, arrow.get(start_time).isoformat())
    close_time = acp_times.close_time(km, distance, arrow.get(start_time).isoformat())
    result = {"open": open_time, "close": close_time}

    return jsonify(result=result)

@app.route('/control_times')
@fresh_login_required
def control_times():
    n = db.control_times.find().count()
    if n == 0:
        return render_template('control_times.html', control_times=None)
    else:
        control_times = db.control_times.find().limit(1)[0]
    
    return render_template('control_times.html', control_times=control_times)

@app.route('/submit_control_times', methods=['POST'])
@fresh_login_required
def submit_control_times():
    control_times = request.json
    if len(control_times["controls"]) != 0:
        db.control_times.drop()
        db.control_times.insert_one(control_times)
    else:
        result = {"status": "No Control Time!"}
        return jsonify(result=result), 204

    result = {"status": "success"}

    return jsonify(result=result)

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=5002, debug=True)
