Case 1 (Passed) for Submit button
If there are no control times, the server will return status code 
204 (No Content), and alert the user "No Control Time!". Also, no control
time will be stored in the database in the case.

Case 2 (Passed) for Display button
If there are no control times stored in the database, the user shall 
not see the results in the new page.